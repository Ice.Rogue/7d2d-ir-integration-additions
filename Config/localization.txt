﻿Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,japanese,german,french

IR_petting_zoo,twitch,Action,,,Petting Zoo,,,,
IR_petting_zooCommand,twitch,Action,,,#spawn_petting_zoo,,,,
IR_petting_zooDesc,twitch,Action,,,Spawns a pack of animals for petting,,,,


IR_the_whole_zooTitle,twitch,Action,,,Release the Whole Zoo,,,,
IR_the_whole_zooCommand,twitch,Action,,,#the_whole_zoo,,,,
IR_the_whole_zooDesc,twitch,Action,,,Spawns a pack of animals for petting and running away from,,,,

IR_all_the_pillsTitle,twitch,Action,,,All the Pills,,,,
IR_all_the_pillsCommand,twitch,Action,,,#all_the_pills,,,,
IR_all_the_pillsActionDesc,twitch,Action,,,Take all the pills to cure fatigue infection and concussion and maybe others,,,,
IR_all_the_pillsBuff,twitch,Buffs,,,All the Pills,,,,
IR_all_the_pillsBuffDesc,twitch,Buffs,,,You took all the pills an are feeling better?,,,,


IR_all_the_bandagesTitle,twitch,Action,,,All the Bandages,,,,
IR_all_the_bandagesCommand,twitch,Action,,,#all_the_bandages,,,,
IR_all_the_bandagesActionDesc,twitch,Action,,,Use all the bandages to stop bleeding cure abraisions and more,,,,
IR_all_the_bandagesBuff,twitch,Buffs,,,All the Pills,,,,
IR_all_the_bandagesBuffDesc,twitch,Buffs,,,You used all the bandages but your insides seem to be staying inside,,,,


IR_walk_it_offTitle,twitch,Action,,,Walk It Off Champ,,,,
IR_walk_it_offCommand,twitch,Action,,,#walk_it_off,,,,
IR_walk_it_offActionDesc,twitch,Action,,,Walk it off what is a few sprains or broken legs to the hero of the apocalypse,,,,
IR_walk_it_offBuff,twitch,Buffs,,,Walk It Off,,,,
IR_walk_it_offBuffDesc,twitch,Buffs,,,You are walking it off and somehow your bones are healing themselves,,,,